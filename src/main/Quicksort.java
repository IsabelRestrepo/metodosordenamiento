package main;

import java.util.Arrays;

public class Quicksort {
    public static void ordenar(int arreglo[], int a, int b) {
        int[] array;
        array = new int[arreglo.length];
        int aux = 0;
        int numFin = 0;
        int inicial = a;
        int fin = b;
        int pivote = arreglo[(inicial + fin) / 2];

        do {

            while (arreglo[inicial] < pivote) {
                inicial++;
            }

            while (arreglo[fin] > pivote) {
                fin--;
            }

            // se hacen los intercambios
            if (inicial <= fin) {
                aux = arreglo[inicial];
                numFin = arreglo[fin];
                arreglo[inicial] = arreglo[fin];
                arreglo[fin] = aux;
                inicial++;
                fin--;

                if (aux != numFin) {
                    System.out.println("\nSe cambia: " + aux + " por " + numFin);
                    System.out.println("" + Arrays.toString(arreglo));
                }
            }
        }
        while (inicial <= fin);
        if (a < fin) {
            ordenar(arreglo, a, fin);

        }
        if (inicial < b) {
            ordenar(arreglo, inicial, b);
        }


    }

    public static void mostrar(int[] arreglo){

        System.out.println("" + Arrays.toString(arreglo));
    }


}


