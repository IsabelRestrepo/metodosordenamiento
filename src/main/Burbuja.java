package main;

import javax.swing.*;
import java.util.Arrays;


public class Burbuja {
    int i, j, auxiliar;

    public void ordenarArreglo(int []numeros) {

        for (i = 0; i < numeros.length - 1; i++){
            for (j = 0; j < numeros.length - i - 1; j++) {
                if (numeros[j + 1] < numeros[j]){
                    auxiliar = numeros[j + 1];
                    numeros[j + 1] = numeros[j];
                    numeros[j] = auxiliar;
                    imprimirDatos(numeros,auxiliar,j);
                }
            }
        }
    }

    public static void imprimirDatos(int []numeros,int auxiliar, int j){
        System.out.println(Arrays.toString(numeros) + " Se cambia "
                + auxiliar + " por " + numeros[j + 1]);
    }
}

